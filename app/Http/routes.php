<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dataEntry', 'dataEntryController@show');
Route::post('/dataEntry/store', 'dataEntryController@store')->name('storeEntry');

Route::get('/dashboard','creativeDashController@show');
Route::post('/dashboard/addNew','creativeDashController@create')->name('addNew');

Route::get('/api/v1/products/{id}',function($id = null){

    $products = App\Product::find($id,array('id','category','name','quantity','price'));
    
    return Response::json(array(
        'error' => false,
        'products' => $products,
        'status_code' => 200
    ));
});

Route::get('/api/v1/products/',function(){
    
    $products = App\Product::all(array('id','category','name','quantity','price'));
    
    return Response::json(array(
        'error' => false,
        'products' => $products,
        'status_code' => 200
    ));
});

Route::get('/facebook', 'facebookConnectController@show');
Route::get('/facebook/callback/', 'facebookConnectController@callback');
