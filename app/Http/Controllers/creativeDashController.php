<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Faker;

use App;
use App\Http\Requests;

class creativeDashController extends Controller
{
    public function show($dashboard = []){
        $creativeDash = App\CreativeDash::get();
        
        foreach ($creativeDash as $key=>$value) {
        $country = App\ClientCountry::where('idClientCountry',$value->countryId)->first();
            array_push($dashboard,[
              'country' => $country->name,
              'name' => $value->name,
              'design' => $value->design,
              'adformat' => $value->adformat,
              'handled_by' => $value->handled_by,
            ]) ;
        }
//        var_dump($collection->country);
        //object form
//        $col = collect($collection);
//        $chunk = $collection->toArray();
        return view('dataManagement.dashboard')->with([
            'dashboard' => $dashboard,
//            'country' => $country,
        ]);
    }
    public function create(Request $request){
        $faker = Faker\Factory::create();
        
        //new country
        $countryID = $faker->randomNumber(5,false);
        $country = new App\ClientCountry;
        $country->idClientCountry = $countryID;
        $country->name = $request->get('country');
        $country->save();
        
        //new creative
        $dash = new App\CreativeDash;
        $dash->idDashboard = $faker->randomNumber(5,false);
        $dash->countryId = $countryID;
        $dash->name = $request->get('nama');
        $dash->design = $request->get('design');
        $dash->adformat = $request->get('adformat');
        $dash->handled_by = $request->get('handledby');
        $dash->save();
        
        return redirect('dashboard');
    }
}
