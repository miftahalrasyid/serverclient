<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Facebook;
use App\FacebookClient;
use App\FacebookUserData;

use App\Http\Requests;

class facebookConnectController extends Controller
{
   
    public function show(){
        $fb = new Facebook\Facebook([
          'app_id' => '644669819061540', // Replace {app-id} with your app id
          'app_secret' => 'a05aa9e0e8e55d76a3f494faabf49efa',
          'default_graph_version' => 'v2.9',
          ]);

        $helper = $fb->getRedirectLoginHelper();
//        $helper = $fb->getJavaScriptHelper();
//        dd($this->helper);
        $permissions = ['email','public_profile','user_friends','user_birthday','user_photos','read_custom_friendlists','user_religion_politics','user_relationships','user_education_history','user_work_history','user_about_me','user_relationship_details','user_location']; // Optional permissions
        $loginUrl = $helper->getLoginUrl(url('/facebook/callback'), $permissions);
        return view('facebookControl.facebookLogin')->with([
            'loginUrl'=> $loginUrl
        ]);
    }
    
    public function callback(){
        $fb = new Facebook\Facebook([
          'app_id' => '644669819061540', // Replace {app-id} with your app id
          'app_secret' => 'a05aa9e0e8e55d76a3f494faabf49efa',
          'default_graph_version' => 'v2.9',
          ]);

        $helper = $fb->getRedirectLoginHelper();
//        $_SESSION['FBRLH_state']=$_GET['state'];
//        echo $_SESSION['helper'];
        try {
          $accessToken = $helper->getAccessToken();
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
          // When Graph returns an error
          echo 'Graph returned an error: ' . $e->getMessage();
          exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
          // When validation fails or other local issues
          echo 'Facebook SDK returned an error: ' . $e->getMessage();
          exit;
        }

        if (! isset($accessToken)) {
          if ($helper->getError()) {
            header('HTTP/1.0 401 Unauthorized');
            echo "Error: " . $helper->getError() . "\n";
            echo "Error Code: " . $helper->getErrorCode() . "\n";
            echo "Error Reason: " . $helper->getErrorReason() . "\n";
            echo "Error Description: " . $helper->getErrorDescription() . "\n";
          } else {
            header('HTTP/1.0 400 Bad Request');
            echo 'Bad request';
          }
          exit;
        }

        // Logged in
        echo '<h3>Access Token</h3>';
        var_dump($accessToken->getValue());

        // The OAuth 2.0 client handler helps us manage access tokens
        $oAuth2Client = $fb->getOAuth2Client();

        // Get the access token metadata from /debug_token
        $tokenMetadata = $oAuth2Client->debugToken($accessToken);
        echo '<h3>Metadata</h3>';
        var_dump($tokenMetadata);
        

        // Validation (these will throw FacebookSDKException's when they fail)
        $tokenMetadata->validateAppId('644669819061540');
//        $tokenMetadata->validateAppId($config['app_id']);
        // If you know the user ID this access token belongs to, you can validate it here
        //$tokenMetadata->validateUserId('123');
        $tokenMetadata->validateExpiration();

        if (! $accessToken->isLongLived()) {
          // Exchanges a short-lived access token for a long-lived one
          try {
            $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
          } catch (Facebook\Exceptions\FacebookSDKException $e) {
            echo "<p>Error getting long-lived access token: " . $e->getMessage() . "</p>\n\n";
            exit;
          }

          echo '<h3>Long-lived</h3>';
          var_dump($accessToken->getValue());
        }

        $_SESSION['fb_access_token'] = (string) $accessToken;
        
        // Since all the requests will be sent on behalf of the same user,
// we'll set the default fallback access token here.
$fb->setDefaultAccessToken($accessToken);

/**
 * Generate some requests and then send them in a batch request.
 */

// Get the name of the logged in user
$requestUserName = $fb->request('GET', '/10207268370186075/friends');
//$requestUserName = $fb->request('GET', '/me?fields=id,name');

// Get user likes
$requestUserLikes = $fb->request('GET', '/me/likes?fields=id,name&limit=1');

// Get user events
$requestUserEvents = $fb->request('GET', '/me/events?fields=id,name&limit=2');

// Post a status update with reference to the user's name
$message = 'My name is {result=user-profile:$.name}.' . "\n\n";
$message .= 'I like this page: {result=user-likes:$.data.0.name}.' . "\n\n";
$message .= 'My next 2 events are {result=user-events:$.data.*.name}.';
$statusUpdate = ['message' => $message];
$requestPostToFeed = $fb->request('POST', '/me/feed', $statusUpdate);

// Get user photos
$requestUserPhotos = $fb->request('GET', '/me/photos?fields=id,source,name&limit=2');

$batch = [
    'user-profile' => $requestUserName,
    'user-likes' => $requestUserLikes,
    'user-events' => $requestUserEvents,
    'post-to-feed' => $requestPostToFeed,
    'user-photos' => $requestUserPhotos,
    ];
//dd($batch);
//        $response = $fb->get('/10207268370186075/friends');
//       dd($response->getGraphUser());
//echo '<h1>Make a batch request</h1>' . "\n\n";
//
try {
    $responses = $fb->sendBatchRequest($batch);
} catch (Facebook\Exceptions\FacebookResponseException $e) {
    // When Graph returns an error
    echo 'Graph returned an error: ' . $e->getMessage();
    exit;
} catch (Facebook\Exceptions\FacebookSDKException $e) {
    // When validation fails or other local issues
    echo 'Facebook SDK returned an error: ' . $e->getMessage();
    exit;
}
//
//foreach ($responses as $key => $response) {
//    if ($response->isError()) {
//        $e = $response->getThrownException();
//        echo '<p>Error! Facebook SDK Said: ' . $e->getMessage() . "\n\n";
//        echo '<p>Graph Said: ' . "\n\n";
//        var_dump($e->getResponse());
//    } else {
//        echo "<p>(" . $key . ") HTTP status code: " . $response->getHttpStatusCode() . "<br />\n";
//        echo "Response: " . $response->getBody() . "</p>\n\n";
//        echo "<hr />\n\n";
//    }
//}
        try {
  // Returns a `Facebook\FacebookResponse` object
  $response = $fb->get('/10207268370186075?fields=birthday,gender,first_name,middle_name,last_name,name,about,devices,education,age_range,favorite_athletes,favorite_teams,inspirational_people,interested_in,languages,political,quotes,relationship_status,religion,sports,work,website', $accessToken->getValue());
} catch(Facebook\Exceptions\FacebookResponseException $e) {
  echo 'Graph returned an error: ' . $e->getMessage();
  exit;
} catch(Facebook\Exceptions\FacebookSDKException $e) {
  echo 'Facebook SDK returned an error: ' . $e->getMessage();
  exit;
}
$result = json_decode($response->getBody());
        echo $result->birthday;
        
dd($result);
//$user = $response->getGraphUser();
//var_dump($user);
//echo 'Name: ' . $user['name'];
        
//store parameter facebookClient
        
        $facebook = new FacebookClient();
        $facebook->token = $accessToken->getValue();
        $facebook->userId = $tokenMetadata->getField("user_id");
        $facebook->save();
        return view('facebookControl.facebookSuccess');
        
        //store parameter facebookUserData
        
        $facebook = new FacebookUserData();
        $facebook->idfacebookUserData = $result->id;
        $facebook->first_name = $result->first_name;
        $facebook->middle_name = $result->middle_name;
        $facebook->last_name = $result->last_name;
        $facebook->name = $result->name;
        $facebook->about = $result->about;
        $facebook->devices = $result->devices;
//        $facebook->idEducation = $result[''];
        $facebook->minimalAge = $result->age_range->min;
        $facebook->relationship_status = $result->relationship_status;
        $facebook->religion = $result->religion;
//        $facebook->idWork = $result[''];
        $facebook->save();
        return view('facebookControl.facebookSuccess');
        
    }
}

/* PHP SDK v5.0.0 */
        /* make the API call */
//        public function __construct(FacebookApp $app = null, $accessToken = null, $method = null, $endpoint = null, array $params = [], $eTag = null, $graphVersion = null)
//        $app = new Facebook\FacebookApp([
//          'app_id' => '644669819061540', // Replace {app-id} with your app id
//          'app_secret' => 'a05aa9e0e8e55d76a3f494faabf49efa',
//        ]);
//        $request = new Facebook\FacebookRequest(
//            $fb->getApp(),
//          $accessToken,
//          'GET',
//          '/{user-id}/friends'
//        );
//        $response = $request->execute();
//        $graphObject = $response->getGraphObject();
//        /* handle the result */
//        var_dump($graphObject);