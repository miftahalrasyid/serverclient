<?php

namespace App\Http\Controllers;

use App\Customer;

use Illuminate\Http\Request;

use App\Http\Requests;

class dataEntryController extends Controller
{
    public function __construct()
    {
        
    }
    
    public function show(){
        return view('dataManagement.dataEntry');
    }
    
    public function store(Request $request){
         $this->validate($request, [
            'username' => 'required|unique:customers|max:255',
            'fname' => 'required',
            'lname' => 'required',
            'email' => 'email|required'
        ]);
        
        $customer = new Customer;

        $customer->username = $request->get('username');
        $customer->firstname = $request->get('fname');
        $customer->lastname = $request->get('lname');
        $customer->email = $request->get('email');
        $customer->reg_date = date('Y-m-d');

        $customer->save();
        
        return view('dataManagement.dataEntry');
    }
}
