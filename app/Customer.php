<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'customers';
    protected $fillable = ['username','firstname','lastname','email','reg_date'];
    public $timestamps = false;
}
