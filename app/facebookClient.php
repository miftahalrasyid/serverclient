<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class facebookClient extends Model
{
    protected $table = 'facebookClient';
    protected $fillable = ['token','userId'];
    public $timestamps = false;
}
