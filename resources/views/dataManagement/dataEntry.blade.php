<html>
    <head>
        <title>Data Entry</title>
        <meta name="description" content="Entry data customer">
        <meta name="keywords" content="Data,Management,Data Management">
        <link rel="stylesheet" href="{{ asset('css/all.css') }}" />
        <link rel="stylesheet" href="{{asset('css/style.css')}}">
        <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    </head>
    <header>
        <h1>Welcome to - Data Management System</h1>
    </header>
    <nav>
    
    </nav>
    <body>
        <section id="customer">
            <div class="col-xs-5">
               
                <form action="{{ route('storeEntry')}}" method="POST">
                    <label for="username">Username</label>
                    <input type="text" class="form-control" id="username" name="username">
                    <label for="fname">First Name</label>
                    <input type="text" class="form-control" id="fname" name="fname">
                    <label for="lname">Last Name</label>
                    <input type="text" class="form-control" id="lname" name="lname">
                    <label for="email">Email</label>
                    <input type="text" class="form-control" id="email" name="email">
                    <input type="submit" id="submit" class="btn btn-primary">
                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                </form>
            </div>
            <div class="col-xs-5">
                 @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </section>
    </body>
    <footer>
    
    </footer>
</html>